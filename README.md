# Automate AWS EKS cluster with Terraform 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)


## Project Description

* Automate provisioning EKS cluster with Terraform 

## Technologies Used 

* Terraform

* AWS EKS 

* Docker 

* Linux 

* Git 

## Steps 

Step 1: Use an existing vpc module for creation of vpc 

[Vpc Module](/images/01_create_configuration_for_vpc_easy_way_of_doing_this_is_using_an_existing_module.png)

Step 2: Define name attribute of vpc 

[Vpc Name](/images/02_define_name_attribute.png)

Step 3: Define vpc CIDR block and use variables instead of hardcoding the value 

[Vpc CIDR](/images/03_define_vpc_cidr_block_use_variables_instead_of_hardcoding_value.png)

Step 4: Define private subnet CIDR block

[Private Subnet CIDR](/images/04_define_private_subnet_cidr_blocks.png)

Step 5: Define public subnet CIDR block 

[Public Subnet CIDR](/images/05_define_public_subnet_cidr_blocks.png)

Step 6: Assign values of variables in the tfvars file 

[Values](/images/06_assign_the_values_in_tfvars_file.png)

Step 7: Specify a region in the provider block this is to enable terraform to know which region you want the subnets to be distributed to 

[Specify Region](/images/07_you_will_need_to_query_the_azs_to_define_the_availability_zone_you_want_the_subnets_to_be_distributed_to_for_us_to_do_this_we_will_need_to_specify_the_region_we_are_quering_the_azs_from_to_do_this_it_will_take_the_region_that_is_defined_in_provider.png)

Step 8: Configure data to query AWS to give all availability zones for the region 

[AZ Data](/images/08_configure_data_to_query_aws_to_give_us_all_availability_zones_for_the_region.png)

Step 9: In VPC module block define the name of the availability zone you want all the subnets to be deployed in, you do this by referencing data in above step name 

[AZ Name](/images/09_define_the_names_of_the_availability_zones_you_want_all_the_subnets_to_be_deployed_in_you_do_this_by_referencing_data_in_above_step_name.png)

Step 10: Enable NAT gateway

[NAT Gateway](/images/10_enable_net_gateway.png)

Step 11: In VPC module block enable single NAT gateway which creates a shared nat gateway for all the private subnets. All the private subnets will route their internet traffic through the single NAT gateway 

[Single NAT Gateway](/images/11_enable_single_net_gateway_which_creates_a_shared_net_gateway_for_all_the_private_subnets_all_the_private_subnets_will-route_their_internet_traffic_through_the_single_NAT_gateway.png)

Step 12: Enable dns host name:

[DNS hostname](/images/12_enable_dns_host_names.png)

Step 13: Set tags to help the cloud control manager identify which vpc and subnet it should connect to 

[tags](/images/13_set_tags_to_help_the_cloud_control_manager_identify_which_vpc_and_subnet_it_should_connect_to.png)

Step 14: Set another tag for public subnet leadbalancing to allow external access to allow external access 

[Public Subnet loadbalancing tag](/images/14_set_another_tag_for_public_subnet_loadbalancing_to_allow_external_access.png)

Step 15: Set a second tag for private subnet to enable internal loadbalancer for services and component inside private subnets

[Private Subnet Internal Loadbalancer tag](/images/15_set_another_tag_for_private_subnet_which_is_internalloadbalancer_for_services_and_component_inside_private_subnet.png)

Step 16: Use terraform init to install vpc module and provider plugin 

    terraform init 

[Install Module and Provider plugin](/images/16_use_terraform_init_to_install_modules_and_provider_plugin.png)

Step 17: Use terraform plan to check for any errors and to see an outline of what is going to be created 

[Terraform Plan error check](/images/17_use_terraform_plan_to_check_for_any_errors_and_an_outline_of_what_is_going_to_be_created.png)

Step 18: Create a file for eks cluster configuration 

    touch eks-cluster.tf 

[eks-cluster file creation](/imsges/18_create_a_file_for_eks_cluster_configuration.png)

Step 19: In the eks file use an eks module to make things faster and easier 

[EKS Module](/images/19_in_the_eks_file_use_an_eks_module_to_make_things_faster_and_simpler.png)

Step 20: Insert cluster name attribute in the eks module block 

[Cluster Name](/images/20_insert_cluster_name_attribute.png)

Step 21: Insert cluster version attribute which is kubernetes version 

[Cluster Version](/images/21_insert_cluster_version_attribute_which_is_kubernetes_version.png)

Step 22: Insert subnet id attribute specifically private subnet so our work isn't exposed, to do this you will have to reference private subnet id from vpc module 

[Subnet id](/images/22_insert_subnet_id_attribute_specifically_private_subnet_so_our_work-isnt_exposed_to_do_this_we_will_have_to_reference_private_subnet_id_from_vpc_module.png)

Step 23: Set tags to enable others identify the cluster 

[tags eks cluster](/images/23_set_tags_to_enable_others_to_identify_the_cluster.png)

Step 24: Set vpc id attribute, you do this by refernecing it from vpc module

[VPC id](/images/24_set_vpc_id_attribute_you_do_this_by_referencing_it_from_vpc_module.png)

Step 25: Set eks manage node group attribute 

[EKS Manage Node G](/images/25_set_eks_manage_node_group_attribute.png)

Step 26: Set Public endpoint to true for eks 

[Public Endpoint](/images/add_set_public_endpoint_to_true_for_eks.png)

Step 27: Perform terraform init to install eks module 

[Install eks module](/images/26_perform_terrafrom_init_to_install_eks_module.png)

Step 28: Perform terraform plan to check for any errors and the components being created

    terraform plan 

[Terraform plan eks](/images/27_perform_terrafrom_plan_to_check_for_any_errors_and_the_components_being_created.png)

Step 29: Apply changes 

    terraform apply -var-file terraform-dev.tfvars

[Apply changes](/images/28_apply_changes.png)

[Apply changes 2](/images/29_apply_changes.png)

Step 30: Confirm if cluster was successfully created

[EKS C on AWS](/images/30_eks_cluster_on_AWS.png)

[EKS C on AWS 2](/images/31_eks_cluster_on_AWS.png)

[EC2 on AWS](/images/32_ec2_instances_on_AWS.png)

Step 30: Connect kubectl to eks cluster 

    aws eks update-kubeconfig --name myapp-eks-cluster --region eu-west-2

[Kubectl connected to eks command](/images/33_connect_kubectl_to_eks_cluster.png)

[Connected to eks ](/images/34_connected_to_eks.png)


## Installation

    brew install terraform 

## Usage 

    Terraform apply 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/terraform-automate-aws-eks-cluster.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/terraform-automate-aws-eks-cluster

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.